
.. index::
   pair: Résistantes; Niloufar Hamedi
   pair: Résistantes; Elaheh Mohammadi
   ! Niloufar Hamedi
   ! Elaheh Mohammadi


.. _niloufar_hamedi:
.. _elaheh_mohammadi:

==========================================
**Niloufar Hamedi et Elaheh Mohammadi**
==========================================

.. figure:: images/niloufar_hamedi_elaheh_mohammadi.png
   :align: center

   https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D


**Niloufar Hamedi et Elaheh Moammadi sont journalistes.**

**Elles ont couvert les premières la mort de Mahsa Jinâ Amini.**

Elles ont été arrêtées en septembre 2022 et risquent la peine de mort
pour avoir exercé leur métier.

**Sans leur courage, cette révolution n'aurait pas eu lieu.**


.. figure:: images/niloufar_hamedi_elaheh_mohammadi_reelles.png
   :align: center


   https://twitter.com/FaridVahiid/status/1653019319079432195#m


Historique
=============

Procès des 29 et 30 mai 2023
--------------------------------

- :ref:`elaheh_mohammadi_2023_05_29`
- :ref:`niloufar_hamedi_2023_05_30`
