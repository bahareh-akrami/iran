
.. index::
   pair: Résistante; Sepideh Qoliyan
   pair: Résistante; Sepideh Gholian
   ! Sepideh Qoliyan
   ! Sepideh Gholian

.. _sepideh_qoliyan:
.. _sepideh_gholian:

=======================================
**Sepideh Gholian (ou Qoliyan)**
=======================================

.. figure:: images/sepideh-qoliyan.png
   :align: center

   https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D



2023-06
==========

- :ref:`iran_2023:sepideh_gholian_2023_09_08`
