
.. index::
   ! Résistantes


.. _resistantes:

==========================
**Résistantes**
==========================

- https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D
- https://nitter.nicfab.eu/Baboobabounette/status/1652311851437285381#m


Pas de semaine d’actualité sur l’Iran cette semaine mais une vidéo `ici <https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D>`_
en entier: https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D


Elles sont l'avant garde de la révolution
=============================================

.. figure:: images/des_resistantes_partout.png
   :align: center

   https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D



.. figure:: images/crop_top.png
   :align: center

   https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D


.. figure:: images/maitres_dechec.png
   :align: center

   https://www.instagram.com/reel/CrnyarWA22z/?igshid=YmMyMTA2M2Y%3D


.. toctree::
   :maxdepth: 3

   jina-mahsa-amini/jina-mahsa-amini
   nika-shakarami/nika-shakarami
   aida-rostami/aida-rostami
   spideh-qalandari/sepideh-qalandari
   niloufar-elaheh/niloufar-elaheh
   shala-abdi/shala-abdi
   sarina-mahmoud-salehi/sarina-mahmoud-salehi
   sepideh-qoliyan/sepideh-qoliyan
   armita-abbasi/armita-abbasi
   ghazal-ramjkesh/ghazal-ramjkesh
   taraneh-alidousti/taraneh-alidousti
