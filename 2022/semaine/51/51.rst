
.. _bahareh_akrami_2022_51:

==============================================================
Semaine 51 Du lundi 19 décembre au dimanche 25 décembre 2022
==============================================================

- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/241222/une-semaine-d-actualite-sur-l-iran-2
- https://femmevieliberte.wixsite.com/my-site/baboo-une-semaine-d-actu-sur-l-iran


Dimanche 25 décembre 2022
=========================


Samedi 24 décembre 2022
=========================

.. figure:: images/samedi_24_decembre_2022_noel.png
   :align: center

   Samedi 24 décembre 2022

Vendredi 23 décembre 2022
=========================

.. figure:: images/vendredi_23_decembre_2022_attentat_terroriste_contre_les_kurdes.png
   :align: center

   Vendredi 23 décembre 2022

.. figure:: images/vendredi_23_decembre_2022_zahedan.png
   :align: center

   Vendredi 23 décembre 2022



Jeudi 22 décembre 2022
=========================

.. figure:: images/jeudi_22_decembre_2022_alpinistes.png
   :align: center

   Jeudi 22 décembre 2022

Mercredi 21 décembre 2022
=========================

.. figure:: images/mercredi_21_decembre_2022_liberation_dena.png
   :align: center

   Mercredi 21 décembre 2022

.. figure:: images/mercredi_21_decembre_2022_reza_shaker.png
   :align: center

   Mercredi 21 décembre 2022

.. figure:: images/mercredi_21_decembre_2022_yalda.png
   :align: center

   Mercredi 21 décembre 2022


Mardi 20 décembre 2022
=========================

.. figure:: images/mardi_20_decembre_2022_onu_nomination_des_membres.png
   :align: center

   Mardi 20 décembre 2022

.. figure:: images/mardi_20_decembre_2022_shoaib.png
   :align: center

   Mardi 20 décembre 2022

.. figure:: images/mardi_20_decembre_2022_manu_terros.png
   :align: center

   Mardi 20 décembre 2022

.. figure:: images/mardi_20_decembre_2022_suite_manu_josep_borrel.png
   :align: center

   Mardi 20 décembre 2022



Lundi 19 décembre 2022
=========================

.. figure:: images/lundi_19_decembre_2022_mollah_ordure.png
   :align: center

   Lundi 19 décembre 2022





