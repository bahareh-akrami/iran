.. index::
   ! Lettres de Baboush ; Baboush numèro 4  (2024-04-15)


.. _baboush_4_2024_05_15:

===============================================================================================================
2024-05-15 **Lettres de Baboush numéro 4 : "la diplomatie des otages » pratiquée par la république islamique"**
===============================================================================================================

- https://x.com/Baboobabounette/status/1790828370684518910
- https://www.instagram.com/p/C7AEgCqrBBh/?utm_source=ig_web_copy_link&igsh=MzRlODBiNWFlZA==
- :ref:`iran_2024:otages_francais`


Cette 4e lettre de Baboush est dédiée à "La diplomatie des otages" pratiquée par la république islamique
===========================================================================================================

.. figure:: images/otages_1.webp

Par exemple, le 18 septembre 2023
======================================

.. figure:: images/otages_2.webp

Mais qui sont les otages ??
==================================


.. figure:: images/otages_3.webp

Et sinon que fait la France ?
=================================

.. figure:: images/otages_4.webp
