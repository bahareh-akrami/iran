.. index::
   ! Lettres de Baboush ; Baboush numèro 5  (2024-05-20)


.. _baboush_5_2024_05_20:

==================================================================================================================================================================
2024-05-20 Lettres de Baboush numéro 5 : **"La mort du president de la republique Ebrahim raïssi et du ministre des affaires etrangères Hossein Amir Abdollahian**
==================================================================================================================================================================

- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/200524/lettres-de-baboush-n-5-la-mort-du-president-de-la-republique-islamique

#helikotlet


.. figure:: images/dessin-1.webp
.. figure:: images/dessin-2.webp
.. figure:: images/dessin-3.webp
.. figure:: images/dessin-4.webp

Liens
========

- :ref:`iran_2024:raissi_2025_05_19`
