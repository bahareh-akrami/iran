.. index::
   Lettres de Baboush ; Baboush N#2  (2024-04-23)


.. _baboush_2024_04_23:

==============================================================================================================
2024-04-23 Lettres de Baboush N#2: **la guerre menée par la république islamique contre les femmes**
==============================================================================================================

- https://blogs.mediapart.fr/bahareh-akrami/blog/230424/lettres-de-baboush-n2-la-guerre-menee-par-la-republique-islamique-contre-les-femmes



Pour l'instant, la guerre est en stand-by
==============================================

.. figure:: images/lettres_de_babouche_1_2024_04_23.webp


Plus de répression à l'intérieur du pays
=============================================

.. figure:: images/lettres_de_babouche_2_2024_04_23.webp



Les frappes en Israël permettent au régime des mollahs de renforcer sa logique sécuritaire
==============================================================================================

.. figure:: images/lettres_de_babouche_3_2024_04_23.webp
