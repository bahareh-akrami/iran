.. index::
   Lettres de Baboush ; Baboush N#1  (2024-04-15)

.. _baboush_2024_04_15:

==================================================================================================
2024-04-15 **Lettres de Baboush N#1 : l’attaque de la République islamique contre Israël**
==================================================================================================

- https://blogs.mediapart.fr/237870/blog/150424/lettres-de-baboush-n1-l-attaque-de-la-republique-islamique-contre-israel


Lettres de Baboush
==========================

.. figure:: images/lettres_de_babouche_1.webp


01 Evidemment cette première lettre est dédié à l'attaque de la république islamique sur Israël
==================================================================================================

.. figure:: images/lettres_de_babouche_2_init.webp


02 Comptes twitter
=================================

- https://nitter.poast.org/jonathanpiron1/rss

.. figure:: images/comptes_twitter.webp


03 Une guerre c'est jamais OUF !
=================================

.. figure:: images/lettres_de_babouche_2.webp


04 Ne confondez pas le régime et le peuple Iranien
=====================================================

.. figure:: images/lettres_de_babouche_3.webp
