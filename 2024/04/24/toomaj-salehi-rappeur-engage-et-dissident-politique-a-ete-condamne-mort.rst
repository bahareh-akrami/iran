.. index::
   ! Toomaj Salehi rappeur engagé et dissident politique a été condamné à mort (2024-04-24)


.. _baboush_2024_04_24:
.. _toomaj_2024_04_24:

==============================================================================================================
2024-04-24 **Toomaj Salehi rappeur engagé et dissident politique a été condamné à mort** |toomaj|
==============================================================================================================

- https://blogs.mediapart.fr/bahareh-akrami/blog/240424/toomaj-salehi-rappeur-engage-et-dissident-politique-ete-condamne-mort
- https://fr.wikipedia.org/wiki/Toomaj_Salehi


#freetoomaj #toomajsalehi #womenlifefreedom #mahsaamini #iranrevolution
#notoexecutionsiniran #notodeathpenalty #irgcterrorists #notoislamicrepublic
#savetoomaj

.. figure:: images/toomaj.webp

Toomaj Salehi, rappeur engagé et militant du mouvement « Femme, Vie, Liberté »,
a été condamné à mort par le tribunal révolutionnaire d’Ispahan, confirme
son avocat Me Amir Raesian.

Ses avocats ont déclaré que cette peine est illégale et qu’ils allaient faire
appel de la décision.

Pour rappel, Toomaj a été arrêté en octobre 2022 à la suite de manifestations
anti régime et a été sévèrement torturé lors de sa détention.

Il avait été libéré sous caution un an après, le 19 novembre 2023.

A sa sortie, Toomaj a immédiatement témoigné par message vidéo des sévices
dont il a été victime en prison, dénonçant la cruauté et corruption du régime.

Cette vidéo lui a valu une nouvelle arrestation violente le 30 novembre 2023.

Depuis, Toomaj est toujours en detention et vient d’être notifié aujourd’hui
de sa condamnation à mort.

Partageons massivement pour exiger la libération immédiate de Toomaj Salehi.


