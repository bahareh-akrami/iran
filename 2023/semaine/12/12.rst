.. index::
   pair: bahareh-akrami ; 2023 semaine 12

.. _bahareh_akrami_2023_12:

================================================================
**Semaine 12 Du lundi 20 mars au dimanche 26 mars 2023**
================================================================


- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/250323/une-semaine-d-actualite-sur-l-iran-13
- https://femmevieliberte.wixsite.com/my-site/baboo-une-semaine-d-actu-sur-l-iran


Samedi 25 mars 2023
=========================

.. figure: images/samedi_11_mars_2023.png
   :align: center



Vendredi 24 mars 2023
=========================

.. figure:: images/vendredi_24_mars_2023_zahedan.png
   :align: center

   Vendredi 24 mars 2023



Jeudi 23 mars 2023
=========================

.. figure:: images/jeudi_23_mars_2023_journee_9_retraites.png
   :align: center

   Jeudi 23 mars 2023


Mercredi 22 mars 2023
=========================


.. figure:: images/mercredi_22_mars_2023_kolbars.png
   :align: center

   Mercredi 22 mars 2023, https://kurdistan.frama.io/luttes/_downloads/8abfb4b17a7ab37a22239bfc1eba7785/bolbar_2023_03_21.pdf



Mardi 21 mars 2023
=========================

.. figure:: images/mardi_21_mars_2023_norouz_deuil.png
   :align: center

   Mardi 21 mars 2023


.. figure:: images/mardi_21_mars_2023_femmes_1401.png
   :align: center

   Mardi 21 mars 2023

Lundi 20 mars 2023 **Norouz à 22h24**
============================================


.. figure:: images/lundi_20_mars_2023_hossein_ali_hossein.png
   :align: center

   Lundi 20 mars 2023


.. figure:: images/lundi_20_mars_2023_norouz_repression.png
   :align: center

   Lundi 20 mars 2023




