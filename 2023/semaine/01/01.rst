.. index::
   pair: bahareh-akrami ; 2023 semaine 1

.. _bahareh_akrami_2023_01:

==============================================================
**Semaine 01 Du lundi 2 janvier au dimanche 8 janvier 2023**
==============================================================

- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/080123/une-semaine-d-actualite-sur-l-iran-4
- https://femmevieliberte.wixsite.com/my-site/baboo-une-semaine-d-actu-sur-l-iran

.. figure:: images/semaine_01_2023.png
   :align: center

   Semaine 01 Du lundi 2 janvier au dimanche 8 janvier 2023


Dimanche 8 janvier 2023
=========================

.. figure:: images/dimanche_8_janvier_2023_ps752.png
   :align: center

   Dimanche 8 janvier 2023


Samedi 7 janvier 2023
=========================

.. figure:: images/samedi_7janvier_2023_seyed.png
   :align: center

   Samedi 7 janvier 2023


Vendredi 6 janvier 2023
=========================

.. figure:: images/vendredi_6_anvier_2023_zahedan.png
   :align: center

   Vendredi 6 janvier 2023


Jeudi 5 janvier 2023
=========================

.. figure:: images/jeudi_5_janvier_2023_orwell.png
   :align: center

   Jeudi 5 janvier 2023

Mercredi 4 janvier 2023
=========================


.. figure:: images/mercredi_4_janvier_2023_charlie_bis.png
   :align: center

   Mercredi 4 janvier 2023

.. figure:: images/mercredi_4_janvier_2023_charlie.png
   :align: center

   Mercredi 4 janvier 2023

.. figure:: images/mercredi_4_janvier_2023_liberation_tarane.png
   :align: center

   Mercredi 4 janvier 2023

Mardi 3 janvier 2023
=========================

.. figure:: images/mardi_3_janvier_2023_kotlet.png
   :align: center

   Mardi 3 janvier 2023

.. figure:: images/mardi_3_janvier_2023_mehdi_karami.png
   :align: center

   Mardi 3 janvier 2023


Lundi 2 janvier 2023
=========================

.. figure:: images/lundi_2_janvier_2023_mehdi_mohammadifard.png
   :align: center

   Lundi 2 janvier 2023










