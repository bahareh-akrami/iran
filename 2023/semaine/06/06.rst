.. index::
   pair: bahareh-akrami ; 2023 semaine 6

.. _bahareh_akrami_2023_06:

================================================================
**Semaine 06 Du lundi 6 février au dimanche 12 février 2023**
================================================================


- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/280123/une-semaine-d-actualite-sur-l-iran-8
- https://femmevieliberte.wixsite.com/my-site/baboo-une-semaine-d-actu-sur-l-iran


Dimanche 12 février 2023
=========================

.. figure: images/dimanche_12_fevrier_2023_mediapart.png
   :align: center

   Dimanche 12 février 2023


Samedi 11 février 2023
=========================

- :ref:`iran_luttes:fariba_adelkhah`

.. figure: images/samedi_11_fevrier_2023_fariba_abdelkhah.png
   :align: center

   Samedi 11 février 2023


.. _bahareh_akrami_2023_02_10:

Vendredi 10 février 2023
=========================

- :ref:`iran_luttes:farhad_meysami_2023_02_02`

.. figure:: images/vendredi_10_fevrier_2023_liberation_fahrad_meissani.png
   :align: center

   Vendredi 10 février 2023


.. figure:: images/vendredi_10_fevrier_2023_zahedan.png
   :align: center

   Vendredi 10 février 2023


.. _bahareh_akrami_2023_02_09:

Jeudi 9 février 2023
=========================

.. figure:: images/jeudi_09_fevrier_2023_niloufar_shakeri.png
   :align: center

   Jeudi 9 février 2023


.. figure:: images/jeudi_09_fevrier_2023_niloufar_hamedi.png
   :align: center

   Jeudi 9 février 2023


Mercredi 8 février 2023
=========================


.. figure:: images/mercredi_08_fevrier_2023_liberation.png
   :align: center

   Mercredi 8 février 2023



.. _armita_abbasi_2023_02_07:

Mardi 7 fevrier 2023
=========================


.. figure:: images/mardi_07_fevrier_2023_armita_abbasi.png
   :align: center

   Mardi 7 fevrier 2023


.. _baraye_2023_02_06:

Lundi 6 fevrier 2023
=========================

- :ref:`iran_luttes:baraye`
- :ref:`iran_luttes:benjamin_briere` |BenjaminBriere|


.. figure:: images/lundi_06_fevrier_2023_toomaj_100_jours.png
   :align: center

   Lundi 6 fevrier 2023


.. figure:: images/lundi_06_fevrier_2023_seisme.png
   :align: center

   Lundi 6 fevrier 2023







