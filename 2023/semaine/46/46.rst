
.. _bahareh_akrami_2023_46:

==============================================================================================================================
**Une semaine d'actualité sur l'iran (Semaine 46 Du lundi 13 septembre au dimanche 19 septembre 2023 par Bahareh Akrami)**
==============================================================================================================================


- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://blogs.mediapart.fr/237870/blog/211123/une-derniere-semaine-d-actualite-sur-l-iran

Une dernière semaine d’actualité sur l’Iran…
==================================================


Lettres de Téhéran (pas accessible pour ceux qui ne supportent pas le réseau d'extrême droite X)
--------------------------------------------------------------------------------------------------

.. figure:: images/semaine_2023_46.webp

Lundi 13 novembre 2023 #MiladZohrehVand
-------------------------------------------

.. figure:: images/lundi_2023_11_13.webp

Mardi 14 novembre 2023
-------------------------------------------

.. figure:: images/mardi_2023_11_14.webp


Mercredi 15 novembre 2023 #NasrinSotoudeh
-------------------------------------------

.. figure:: images/mercredi_2023_11_15.webp


Vendredi 17 novembre 2023
-------------------------------------------

.. figure:: images/vendredi2023_11_17.webp


Samedi  18 novembre 2023
-------------------------------------------

.. figure:: images/samedi_2023_11_18.webp

