.. index::
   ! Bahareh Akrami
   ! Baboo

.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/feminisme.rss
.. https://framapiaf.org/web/tags/MarchepourlEgalité.rss

.. un·e


|FluxWeb| `RSS <https://bahareh-akrami.frama.io/iran/rss.xml>`_

.. _dessins_bahareh_akrami:

=========================================
**Dessins de Bahareh Akrami (baboo)**
=========================================

- https://blogs.mediapart.fr/bahareh-akrami/portfolios
- https://mastodon.social/@bahareh_akrami
- https://www.instagram.com/baboo_chamailleuse/
- https://www.liberation.fr/portraits/bahareh-akrami-dessiner-les-vivants-20220624_5X2PETI5WREZLMTGCJZBPK6D44/

.. figure:: images/bahareh_akrimi.png
   :align: center
   :width: 300

   Bahareh Akrami


.. figure:: images/baboo.png
   :align: center

   Baboo, https://blogs.mediapart.fr/bahareh-akrami/portfolios


.. figure:: images/baboo_instagram.png
   :align: center

   Baboo chamailleuse, https://www.instagram.com/baboo_chamailleuse/


.. figure:: images/baboo_twitter.png
   :align: center

   Baboo sur l'oiseau mort, https://nitter.at/Baboobabounette, https://nitter.at/Baboobabounette/rss


Ils se trompent, ceux qui croient en la générosité du destin.

Ignorent-ils à quel point ce dernier est capable de se jouer des humains ?
Bahareh Akrami, elle, sait. Elle l’a raconté, un soir de septembre, à la
barre du procès des attentats du 13 Novembre, en choisissant ses mots :
«Je suis une double rescapée.»

**Par deux fois, d’abord en Iran où elle est née, puis en France, où sa
famille s’est réfugiée, sa vie a été heurtée par l’obscurantisme religieux**.

Par deux fois, **Bahareh Akrami a échappé à la violence de l’islamisme radical**


.. toctree::
   :maxdepth: 5

   resistantes/resistantes
   2024/2024
   2023/2023
   2022/2022
   presentations/presentations
