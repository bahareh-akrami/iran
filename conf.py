# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Dessins de Bahareh Akrami sur l'Iran"
html_title = project

author = f"Human people"
html_logo = "images/baboo.png"
html_favicon = "images/bahareh_akrami_logo.png"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Iran"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    # "iran_culture": ("https://iran.frama.io/culture/", None),
    "iran_exposition": ("https://iran.frama.io/exposition/", None),
    "iran_luttes": ("https://iran.frama.io/luttes/", None),
    "iran_2023": ("https://iran.frama.io/luttes-2023/", None),
    "iran_2024": ("https://iran.frama.io/luttes-2024/", None),
    "tuto_fediverse": ("https://gdevops.frama.io/web/fediverse/", None),
    "rojava_luttes": ("https://rojava.frama.io/luttes/", None),
    "kurdistan_luttes": ("https://kurdistan.frama.io/luttes/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True


# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://bahareh-akrami.frama.io/iran/",
    "repo_url": "https://framagit.org/bahareh-akrami/iran",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "green",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://iran.frama.io/exposition",
            "internal": False,
            "title": "Exposition Iran",
        },
        {
            "href": "https://iran.frama.io/luttes/",
            "internal": False,
            "title": "Iran luttes",
        },
    ],
    "heroes": {
        "index": "Dessins de Bahareh Akrami",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"-2500-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |JinaAmini| image:: /images/mahsa_jina_amini_avatar.png
.. |HadisNajafi| image:: /images/hadis_najafi_avatar.png
.. |AidaRostami| image:: /images/aida_rostami_avatar.png
.. |NikaShakarimi| image:: /images/nina_shakarami_avatar.png
.. |SarinaEsmaeilzadeh| image:: /images/sarina_esmaeilzadeh_avatar.png
.. |NagihanAkarsel| image:: /images/nagihan_akarsel_avatar.png
.. |ElnazKerabi| image:: /images/elnaz_kerabi_avatar.png
.. |ps752justice| image:: /images/ps752_justice.png
.. |BerivanFirat| image:: /images/berivan_firat_avatar.png
.. |KianPirfalak| image:: /images/kian_pirfalak_avatar.jpeg
.. |Anonymous| image:: /images/anonymous_avatar.png
.. |OnuFemmes| image:: /images/onu_femmes_avatar.png
.. |ici_grenoble| image:: /images/ici_grenoble_avatar.png
.. |EmineKara| image:: /images/emine_kara_avatar.jpg
.. |AbdurrahmanKizil| image:: /images/abdurrahman_kizil_avatar.jpg
.. |MirPerwer| image:: /images/mir_perwer_avatar.png
.. |MehdiKarami| image:: /images/mehdi_karami_avatar.png
.. |MohammadHosseini| image:: /images/mohammad_hosseini_avatar.png
.. |MohsenShekari| image:: /images/mohsen_shekari_avatar.png
.. |MajidRezaRahnavard| image:: /images/majid_reza_rahnavard_avatar.png
.. |BenjaminBriere| image:: /images/benjamin_briere_avatar.png
.. |BernardPhelan| image:: /images/bernard_phelan_avatar.png
.. |FaribaAdelkhah| image:: /images/fariba_adelkhah_avatar.png
.. |CecileKohler| image:: /images/cecile_kohler_avatar.png
.. |JacquesParis| image:: /images/jacques_paris_avatar.png
.. |LouisArnaud| image:: /images/louis_arnaud_avatar.png
.. |toomaj| image:: /images/toomaj_salehi_avatar.webp
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
